package org.example;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;
import java.util.List;

public class demandeEntreeUtilisateur {

    static Scanner scanner = new Scanner(System.in);

    public static String saisiePhrase() {
        /** Je demande à l'utilisateur la phrase qu'il veut vérifier */
        System.out.print("Entrez une phrase : ");
        String phraseUtilisateur = scanner.nextLine();
        return phraseUtilisateur;
    }

    public static String lettreUtilisateur() {
        /** Je demande à l'utilisateur la lettre qu'il veut vérifier */
        System.out.print("Quelle lettre recherchez-vous ? : ");
        String lettreUtilisateur = scanner.nextLine();
        return lettreUtilisateur;
    }

    public static void chercheLettre(String phrase, String lettre) {
        /** Je cherche si la lettre donnée se retrouve dans la phrase donnée par l'utilisateur */
        int compteur = 0;
        int chercheLettre = 0;

        for (int positionLettre = 0; positionLettre < phrase.length(); positionLettre++) {
            if (phrase.charAt(positionLettre) == lettre.charAt(0)) {
                chercheLettre++;
                System.out.println("La lettre " + lettre + " se trouve à la position " + (positionLettre+1) + " dans la phrase");//je rajoute +1 à positionLettre car les positions commencent à 0)
            }
        }

        if (chercheLettre == 0) {
                   System.out.println("La lettre " + lettre + " n'est pas dans la phrase.");
        }
    }

}


